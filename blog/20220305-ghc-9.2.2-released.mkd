---
author: bgamari
title: "GHC 9.2.2 is now available"
date: 2022-03-06
tags: release
---

The GHC developers are very happy to at announce the availability of GHC
9.2.2. Binary distributions, source distributions, and documentation are
available at [`downloads.haskell.org`](https://downloads.haskell.org/ghc/9.2.2).

This release includes many bug-fixes and other improvements to 9.2.1 including:
 
 * A number of bug-fixes in the new AArch64 native code generator

 * Fixes ensuring that the `indexWord8ArrayAs*#` family of primops is handled
   correctly on platforms lacking support for unaligned memory accesses
   (#21015, #20987).

 * Improvements to the compatibility story in GHC's migration to the
   XDG Base Directory Specification (#20684, #20669, #20660)

 * Restored compatibility with Windows 7

 * A new `-fcompact-unwind` flag, improving compatibility with C++ libraries on
   Apple Darwin (#11829)

 * Introduction of a new flag, `-fcheck-prim-bounds`, enabling runtime bounds
   checking of array primops (#20769)

 * Unboxing of unlifted types (#20663)

 * Numerous improvements in compiler performance.

 * Many, many others. See the [release notes][] for a full list.

As some of the fixed issues do affect correctness users are encouraged to
upgrade promptly.

Finally, thank you to Microsoft Research, GitHub, IOHK, the Zw3rk stake
pool, Tweag I/O, Serokell, Equinix, SimSpace, and other anonymous
contributors whose on-going financial and in-kind support has
facilitated GHC maintenance and release management over the years.
Moreover, this release would not have been possible without the hundreds
of open-source contributors whose work comprise this release.

As always, do open a [ticket][] if you see anything amiss.

Happy compiling,

- Ben


[ticket]: https://gitlab.haskell.org/ghc/ghc/-/issues/new
[release notes]: https://downloads.haskell.org/ghc/9.2.2/docs/html/users_guide/9.2.2-notes.html
