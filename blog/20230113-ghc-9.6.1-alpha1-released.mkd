---
author: bgamari
title: "GHC 9.6.1-alpha1 is now available"
date: 2023-01-13
tags: release
---

The GHC team is very pleased to announce the availability of GHC 9.6.1-alpha1.
As usual, binaries and source distributions are available at
[downloads.haskell.org](https://downloads.haskell.org/ghc/9.6.1-alpha1/).
This is the first alpha release in the 9.6 series which will bring a number of
exciting features:

* A new Javascript code generation backend

* A new WebAssembly code generation backend,

* Significant latency improvements in the non-moving garbage collector

* Support for loading of multiple components in GHCi

* Efficient support for delimited continuations

* Improvements in error messages

* Numerous improvements in compiler-residency

Note that both the Javascript and WebAssembly backends are still in a state of
infancy and are present in this release as a technology preview; we hope that
they will mature considerably before the final 9.6.1 release.

Please give this release a try and open a [ticket][] if you see anything
amiss.

Cheers,

- Ben


[ticket]: https://gitlab.haskell.org/ghc/ghc/issues/
