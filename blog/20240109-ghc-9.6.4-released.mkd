---
author: Zubin Duggal
title: "GHC 9.6.4 is now available"
date: 2024-01-09
tags: release
---

The GHC developers are happy to announce the availability of GHC 9.6.4. Binary
distributions, source distributions, and documentation are available on the
[release page](/download_ghc_9_6_4.html).

This release is primarily a bugfix release addressing a few issues
found in the 9.6 series. These include:

* A fix for a bug where certain warnings flags were not recognised (#24071)
* Fixes for a number of simplifier bugs (#23952, #23862).
* Fixes for compiler panics with certain package databases involving unusable
  units and module reexports (#21097, #16996, #11050).
* A fix for a typechecker crash (#24083).
* A fix for a code generator bug on AArch64 platforms resulting in invalid
  conditional jumps (#23746).
* Fixes for some memory leaks in GHCi (#24107, #24118)
* And a few other fixes

A full accounting of changes can be found in the [release notes]. As
some of the fixed issues do affect correctness users are encouraged to
upgrade promptly.

We would like to thank Microsoft Azure, GitHub, IOG, the Zw3rk stake pool,
Well-Typed, Tweag I/O, Serokell, Equinix, SimSpace, Haskell Foundation, and
other anonymous contributors whose on-going financial and in-kind support has
facilitated GHC maintenance and release management over the years. Finally,
this release would not have been possible without the hundreds of open-source
contributors whose work comprise this release.

As always, do give this release a try and open a [ticket][] if you see
anything amiss.

Enjoy!

-Zubin

[ticket]: https://gitlab.haskell.org/ghc/ghc/-/issues/new
[release notes]: https://downloads.haskell.org/~ghc/9.6.4/docs/users_guide/9.6.4-notes.html
