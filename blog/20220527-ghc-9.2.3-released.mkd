---
author: Zubin Duggal
title: "GHC 9.2.3 is now available"
date: 2022-05-27
tags: release
---

The GHC developers are very happy to at announce the availability of GHC
9.2.3. Binary distributions, source distributions, and documentation are
available at [`downloads.haskell.org`](https://downloads.haskell.org/ghc/9.2.3).

This release includes many bug-fixes and other improvements to 9.2.2 including:

 * A fix to a critical linking bug on Windows causing the 9.2.2 release to be unusable (#21196).

 * Numerous bug fixes for regressions and other issues in the typechecker (#2147, #21515, #20582, #20820, #21130, #21479).

 * Correctness bugs in the code generator and compiler backends (#21396, #20959, #21465).

 * Packaging fixes on many platforms (#20760, #21487, #21402).

 * Many others. See the [release notes][] for a full list.

As some of the fixed issues do affect correctness users are encouraged to
upgrade promptly.

We would like to thank Microsoft Azure, GitHub, IOG, the Zw3rk
stake pool, Tweag I/O, Serokell, Equinix, SimSpace, and other anonymous
contributors whose on-going financial and in-kind support has
facilitated GHC maintenance and release management over the years.
Finally, this release would not have been possible without the hundreds
of open-source contributors whose work comprise this release.

As always, do give this release a try and open a [ticket] if you see
anything amiss.

Happy compiling,

- Zubin


[ticket]: https://gitlab.haskell.org/ghc/ghc/-/issues/new
[release notes]: https://downloads.haskell.org/ghc/9.2.3/docs/html/users_guide/9.2.3-notes.html
