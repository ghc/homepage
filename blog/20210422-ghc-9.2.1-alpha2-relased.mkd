---
author: Ben Gamari
categories: release
date: '2021-04-22'
title: GHC 9.2.1-alpha2 now available
---

The GHC developers are very happy to announce the availability of the second
alpha release in the 9.2.1 series. Binary distributions, source distributions,
and documentation are available from
[downloads.haskell.org](https://downloads.haskell.org/ghc/9.2.1-alpha2).

GHC 9.2 will bring a number of exciting features including:

 * Many changes in the area of records, including the new
   `RecordDotSyntax` and `NoFieldSelectors` language extensions, as well
   as Support for `DuplicateRecordFields` with `PatternSynonyms`.

 * Introduction of the new `GHC2021` language extension set, giving
   users convenient access to a larger set of language extensions which
   have been long considered stable.

 * Merge of `ghc-exactprint` into the GHC tree, providing infrastructure
   for source-to-source program rewriting out-of-the-box.

 * Introduction of a `BoxedRep` `RuntimeRep`, allowing for polymorphism
   over levity of boxed objects (#17526)

 * Implementation of the [`UnliftedDataTypes` extension][proposal], allowing
   users to define types which do not admit lazy evaluation

 * The new [-hi profiling][] mechanism which provides significantly
   improved insight into thunk leaks.

 * Support for the [`ghc-debug`][ghc-debug] out-of-process heap inspection
   library

 * Support for profiling of pinned objects with the cost-centre profiler
   (#7275)

 * Introduction of Haddock documentation support in TemplateHaskell (#5467)

 * Proper support for impredicative types in the form of Quick-Look
   impredicativity.

 * A native code generator backend for AArch64.

This pre-release brings nearly 50 fixes relative to the first alpha,
although the long-awaited ARM NCG backend hasn't quite landed yet.

As always, do give this a try and open a [ticket] if you see anything amiss.

Happy testing!

[proposal]: https://github.com/ghc-proposals/ghc-proposals/blob/master/proposals/0265-unlifted-datatypes.rst
[-hi profiling]: https://well-typed.com/blog/2021/01/first-look-at-hi-profiling-mode/
[ghc-debug]: http://ghc.gitlab.haskell.org/ghc-debug/
[ticket]: https://gitlab.haskell.org/ghc/ghc/-/issues/new
