---
author: bgamari
title: "GHC 9.10.1-alpha2 is now available"
date: 2024-03-29
tags: release
---

The GHC developers are very pleased to announce the availability
of the second alpha release of GHC 9.10.1. Binary distributions, source
distributions, and documentation are available at [downloads.haskell.org][].

We hope to have this release available via ghcup shortly.

GHC 9.10 will bring a number of new features and improvements, including:

 * The introduction of the `GHC2024` language edition, building upon
   `GHC2021` with the addition of a number of widely-used extensions.

 * Partial implementation of the [GHC Proposal #281][], allowing visible
   quantification to be used in the types of terms.

 * [Extension][linear let] of LinearTypes to allow linear `let` and `where`
   bindings

 * The implementation of the [exception backtrace proposal][exception
   backtrace], allowing the annotation of exceptions with backtraces, as well
   as other user-defined context

 * Further improvements in the info table provenance mechanism, reducing
   code size to allow IPE information to be enabled more widely

 * Javascript FFI support in the WebAssembly backend

 * Improvements in the fragmentation characteristics of the low-latency
   non-moving garbage collector.

 * ... and many more

A full accounting of changes can be found in the [release notes][].
As always, GHC's release status, including planned future releases, can
be found on the GHC Wiki [status][].

We would like to thank GitHub, IOG, the Zw3rk stake pool,
Well-Typed, Tweag I/O, Serokell, Equinix, SimSpace, the Haskell
Foundation, and other anonymous contributors whose on-going financial
and in-kind support has facilitated GHC maintenance and release
management over the years. Finally, this release would not have been
possible without the hundreds of open-source contributors whose work
comprise this release.

As always, do give this release a try and open a [ticket][] if you see
anything amiss.


[release notes]: https://downloads.haskell.org/ghc/9.10.1-alpha2/docs/users_guide/9.10.1-notes.html
[status]: https://gitlab.haskell.org/ghc/ghc/-/wikis/GHC-status
[exception backtrace]: https://github.com/ghc-proposals/ghc-proposals/pull/330
[GHC Proposal #281]: https://github.com/ghc-proposals/ghc-proposals/pull/281
[linear let]: https://github.com/ghc-proposals/ghc-proposals/pull/624
[downloads.haskell.org]: https://downloads.haskell.org/ghc/9.10.1-alpha2
[ticket]: https://gitlab.haskell.org/ghc/ghc/-/issues/new
