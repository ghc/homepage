---
author: bgamari
title: "GHC 8.10.1-rc1 now available"
date: 2020-01-24
tags: release
---

The GHC team is happy to announce the availability of the first release
candidate of GHC 8.10.1. Source and binary distributions are
available at the [usual place](https://downloads.haskell.org/ghc/8.10.1-rc1/).

GHC 8.10.1 will bring a number of new features including:

 * The new `UnliftedNewtypes` extension allowing newtypes around unlifted
   types.

 * The new `StandaloneKindSignatures` extension allows users to give
   top-level kind signatures to type, type family, and class
   declarations.

 * A new warning, `-Wderiving-defaults`, to draw attention to ambiguous
   deriving clauses

 * A number of improvements in code generation, including changes

 * A new GHCi command, `:instances`, for listing the class instances
   available for a type.

 * An upgraded Windows toolchain lifting the `MAX_PATH` limitation

 * A new, low-latency garbage collector.

 * Improved support profiling, including support for sending profiler
   samples to the eventlog, allowing correlation between the profile and
   other program events

This is the first and likely final release candidate. For a variety of
reasons, it comes a few weeks later than the original schedule of
release late December. However, besides a few core libraries
book-keeping issues this candidate is believed to be in good condition
for the final release. As such, the final 8.10.1 release will likely
come in two weeks.

Note that at the moment we still require that macOS Catalina users
exempt the binary distribution from the notarization requirement by
running `xattr -cr .` on the unpacked tree before running `make install`.

In addition, we are still looking for any Alpine Linux to help diagnose
the correctness issues in the [Alpine binary distribution][alpine]. If you use
Alpine any you can offer here would be greatly appreciated.

Please do test this release and let us know if you encounter any other
issues.



[alpine]: https://gitlab.haskell.org/ghc/ghc/issues/17508
[notarization]: https://gitlab.haskell.org/ghc/ghc/issues/17418
