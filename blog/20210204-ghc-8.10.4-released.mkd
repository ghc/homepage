---
author: bgamari
title: "GHC 8.10.4 is now available"
date: 2021-02-05
tags: release
---

The GHC team is very pleased to announce the availability of GHC 8.10.4.
Source and binary distributions are available at the [usual
place](https://downloads.haskell.org/ghc/8.10.4/).

This is a small bug-fix release, fixing two bugs present in 8.10.3:

* Fix a linker hang triggered by dynamic code loading on Windows
  (#19155)

* Fix a crash caused by inappropriate garbage of heap-allocated data reachable
  from foreign exports (#19149)

As always, feel free to report any issues you encounter via
[gitlab.haskell.org](https://gitlab.haskell.org/ghc/ghc/-/issues/new).
