---
author: bgamari
title: "GHC 8.8.2 released"
date: 2020-01-16
tags: release
---

The GHC team is proud to announce the release of GHC 8.8.2. The source
distribution, binary distributions, and documentation are available at
[downloads.haskell.org](https://downloads.haskell.org/~ghc/8.8.2). Release
notes are also
[available](https://downloads.haskell.org/ghc/8.8.2/docs/html/users_guide/8.8.2-notes.html)

This release fixes a handful of issues affecting 8.8.1:

 - A bug (#17088) in the compacting garbage collector resulting in
   segmentations faults under specific circumstances. Note that this may
   affect user programs even if they did not explicitly request the
   compacting GC (using the -c RTS flag) since GHC may fallback to
   compacting collection during times of high memory pressure.

 - A code generation bug (#17334) resulting in GHC panics has been
   fixed.

 - A bug in the `process` library causing builds using `hsc2hs` to fail
   non-deterministically on Windows has been fixed (#17480)

 - A typechecker bug (#16288) resulting in programs being unexpectedly
   rejected has been fixed.

 - A bug in the implementation of compact normal forms resulting in
   segmentation faults in some uses (#17044) has been fixed.

 - A bug causing GHC to incorrectly complain about incompatible LLVM
   versions when using LLVM 7.0.1 has been fixed (#16990).

As always, if anything looks amiss do let us know.

Happy compiling!

