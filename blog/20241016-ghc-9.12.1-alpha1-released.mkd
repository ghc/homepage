---
author: Zubin Duggal
title: "GHC 9.12.1-alpha1 is now available"
date: 2024-10-16
tags: release
---

The GHC developers are very pleased to announce the availability
of the first alpha release of GHC 9.12.1. Binary distributions, source
distributions, and documentation are available at [downloads.haskell.org][].


We hope to have this release available via ghcup shortly.

GHC 9.12 will bring a number of new features and improvements, including:

 * The new language extension [OrPatterns] allowing you to combine multiple
   pattern clauses into one.

 * The [MultilineStrings] language extension to allow you to more easily write
   strings spanning multiple lines in your source code.

 * Improvements to the OverloadedRecordDot extension, allowing the built-in
   `HasField` class to be used for records with fields of non lifted representations.

 * The [NamedDefaults] language extension has been introduced allowing you to
   define defaults for typeclasses other than `Num`.

 * More deterministic object code output, controlled by the `-fobject-determinism`
   flag, which improves determinism of builds a lot (though does not fully do so)
   at the cost of some compiler performance (1-2%). See #12935 for the details

 * GHC now accepts type syntax in expressions as part of [GHC Proposal #281].

 * ... and many more

A full accounting of changes can be found in the [release notes][].
As always, GHC's release status, including planned future releases, can
be found on the GHC Wiki [status][].

We would like to thank GitHub, IOG, the Zw3rk stake pool,
Well-Typed, Tweag I/O, Serokell, Equinix, SimSpace, the Haskell
Foundation, and other anonymous contributors whose on-going financial
and in-kind support has facilitated GHC maintenance and release
management over the years. Finally, this release would not have been
possible without the hundreds of open-source contributors whose work
comprise this release.

As always, do give this release a try and open a [ticket][] if you see
anything amiss.


[release notes]: https://downloads.haskell.org/ghc/9.12.1-alpha1/docs/users_guide/9.12.1-notes.html
[status]: https://gitlab.haskell.org/ghc/ghc/-/wikis/GHC-status
[downloads.haskell.org]: https://downloads.haskell.org/ghc/9.12.1-alpha1
[ticket]: https://gitlab.haskell.org/ghc/ghc/-/issues/new
[OrPatterns]: https://github.com/ghc-proposals/ghc-proposals/blob/master/proposals/0522-or-patterns.rst
[MultilineStrings]: https://github.com/ghc-proposals/ghc-proposals/blob/master/proposals/0569-multiline-strings.rst
[GHC Proposal #281]: https://github.com/ghc-proposals/ghc-proposals/blob/master/proposals/0281-visible-forall.rst
[NamedDefaults]: https://github.com/ghc-proposals/ghc-proposals/blob/master/proposals/0409-exportable-named-default.rst
