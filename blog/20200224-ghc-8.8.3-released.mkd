---
author: bgamari
title: "GHC 8.8.3 released"
date: 2020-02-24
tags: release
---

The GHC team is proud to announce the release of GHC 8.8.3. The source
distribution, binary distributions, and documentation are available at
[downloads.haskell.org](https://downloads.haskell.org/~ghc/8.8.3)

Release notes are also [available][1].

This release fixes a handful of issues affecting 8.8.2:

 - a compiler panic (#17429) due to over-zealous eta reduction

 - a linker crash on Windows (#17575)

 - the ability to bootstrap with earlier 8.8 releases has been restored
   (#17146)

 - the `directory` submodule has been updated, fixing #17598

As always, if anything looks amiss do let us know.


[1]: https://downloads.haskell.org/ghc/8.8.3/docs/html/users_guide/8.8.3-notes.html

