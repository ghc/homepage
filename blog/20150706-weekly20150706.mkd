---
author: bgamari
title: "GHC Weekly News - 2015/07/06"
date: 2015-07-06
tags: 
---

Hi \*,

Welcome for the latest entry in the GHC Weekly News.  The past week, GHC HQ met up to discuss the status of the approaching 7.10.2 release.


# 7.10.2 status

After quite some delay due to a number of tricky regressions in 7.10.1, 7.10.2 is nearing the finish line. Austin cut release candidate 2 on Friday and so far the only reports of trouble appear to be some validation issues, most of which have already been fixed thanks to Richard Eisenberg.

7.10.2 will include a number of significant bug-fixes. These include,

 * #10521, where overlap of floating point STG registers weren't properly accounted for, resulting in incorrect results in some floating point computations. This was fixed by the amazing Reid Barton.
 * #10534, a type-safety hole enabling a user to write `unsafeCoerce` with data families and `coerce`. Fix due to the remarkable Richard Eisenberg.
 * #10538, where some programs would cause the simplifier to emit an empty case, resulting in runtime crashes. Fix due to the industrious Simon Peyton Jones.
 * #10527, where the simplifier would expend a great deal of effort simplifying arguments which were never demanded by the callee.
 * #10414, where a subtle point of the runtime system's black-holing mechanism resulting in hangs on a carefully constructed testcase.
 * #10236, where incorrect DWARF annotations would be generated, resulting in incorrect backtraces. Fixed by Peter Wortmann
 * #7450, where cached free variable information was being unnecessarily dropped by the specialiser, resulting in non-linear compile times for some programs.

See the [status page](https://ghc.haskell.org/trac/ghc/wiki/Status/GHC-7.10.2) for a complete listing of issues fixed in this release.

In the coming days we will being working with FP Complete to test the pre-release against Stackage. While Hackage tends to be too large to build in bulk, the selection of packages represented in Stackage is feasible to build and is likely to catch potential regressions. Hopefully this sort of large-scale validation will become common-place for future releases.

If all goes well, 7.10.2 will mark the end of the 7.10 series. However, there is always the small possibility that a major regression will be found. In this case we will cut a 7.10.3 release which will include a few [patches](https://ghc.haskell.org/trac/ghc/query?status=merge&milestone=7.10.3&col=id&col=summary&col=status&col=type&col=priority&col=milestone&col=component&report=19&order=priority) which didn't make it into 7.10.2.

# Other matters

It has been suggested in #10601 that GHC builds should ship with DWARF symbols for the base libraries and runtime system. While we all want to see this new capability in users' hands, 7.10.2 will, like 7.10.1, not be shipping with debug symbols. GHC HQ will be discussing the feasibility of including debug symbols with 7.12 in the future. In the meantime, we will be adding options to `build.mk` to make it easier for users to build their own compilers with debug-enabled libraries.

In this week's GHC meeting the effort to port GHC's build system to the
[[Shake]] build system briefly came up. Despite the volume of updates on the
[Wiki](https://ghc.haskell.org/trac/ghc/wiki/Building/Shake) Simon reports that
the project is still moving along. The current state of the Shake-based build
system can be found on
[Github](https://github.com/snowleopard/shaking-up-ghc/tree/master).

While debugging #7540 it became clear that there may be trouble lurking in the
profiler. Namely when profiling GHC itself `lintAnnots` is showing up strongly
where it logically should not. This was previously addressed in #10007, which
was closed after a patch by Simon Marlow was merged. As it appears that this
did not fully resolve the issue I'll be looking further into this.

~ Ben
