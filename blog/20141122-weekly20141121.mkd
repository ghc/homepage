---
author: thoughtpolice
title: "GHC Weekly News - 2014/11/21"
date: 2014-11-22
tags: ghc news
---

Hi \*,

To get things back on track, we have a short post following up the earlier one this week. It's been busy today so I'll keep it short:

  - The STABLE freeze Austin announced two weeks ago is happening now, although at this point a few things we wanted to ship are just 98% ready. So it may wait until Monday.

  - HEAD has been very busy the past two days as many things are now trying to merge as closely to the window as possible. Some notes follow.

  - Gergo Erdi merged the implementation of pattern synonym type signatures: https://www.haskell.org/pipermail/ghc-devs/2014-November/007369.html

  - HEAD now has support for using the 'deriving' clause for arbitrary classes (see #5462).

  - HEAD now has a new flag `-fwarn-missing-exported-sigs`, which fixes #2526. See https://phabricator.haskell.org/D482

  - HEAD now has 64bit iOS and SMP support for ARM64, thanks to Luke Iannini. See #7942.

  - HEAD no longer ships `haskell98`, `haskell2010`, `old-locale` or `old-time`, per our decision to drop support for `haskell98` and `haskell2010`. GHC 7.10 compatible releases of `old-locale` and `old-time` have been released on hackage. See https://www.haskell.org/pipermail/ghc-devs/2014-November/007357.html and https://www.haskell.org/pipermail/ghc-devs/2014-November/007383.html

  - `base` now exports a new module for Natural numbers called `Numeric.Natural` following Herbert Valerio Riedel's recent proposal.

  - HEAD should finally be compatible with LLVM 3.5, AKA #9142. The patch from Ben Gamari is at https://phabricator.haskell.org/D155

  - Your author has been busy and delayed due to some bad travel experiences the past week, so the 7.8.4 RC1 hasn't landed this past week. Hopefully it will be out by the end of this week still.

Since the last update was only a few days ago, you'd think we haven't closed a lot of tickets, but we have! Thomas Miedema has been very very persistent about closing tickets and cleaning them up, which is greatly appreciated: #9810, #8324, #8310, #9396, #9626, #9776, #9807, #9698, #7942, #9703, #8584, #8968, #8174, #9812, #9209, #9220, #9151, #9201, #9318, #9109, #9126, #8406, #8102, #8093, #8085, #8068, #8094, #9590, #9368, #2526, #9569, #8149, #9815, #5462, #9647, #8568, #9293, #7484, #1476, #9824, #9628, #7942
