---
title: Reporting security disclosures
---

Security Disclosure Policy
==========================

If you have found a bug in the GHC distribution which you believe may have
security implications, please report by email to
[ghc-disclosures@haskell.org](mailto:ghc-disclosures@haskell.org). This will be
received by a small group of core developers who can work to promptly and
responsibly resolve the issue. Sensitive disclosures should ideally be GPG
encrypted using [our public key](/ghc-security-key.pgp.ascii)
([`keyserver.ubuntu.com`][keyserver]).

Please do use the keyword `[DISCLOSURE]` in the subject line to ensure that
your message is not missed. The GHC Security Team will respond with an
acknowledgement to your report within 48 hours.

Thank you for your help in keeping the Haskell community safe!

[keyserver]: http://keyserver.ubuntu.com/pks/lookup?search=GHC+Security+Disclosures&fingerprint=on&op=index
