---
title: Glasgow Haskell Compiler
---

Latest News
-----------

16 Dec 2024
:   GHC 9.12.1 Released! \[[download](download_ghc_9_12_1.html)\]

2 Dec 2024
:   GHC 9.8.4 Released! \[[download](download_ghc_9_8_4.html)\]

20 Oct 2024
:   GHC 9.8.3 Released! \[[download](download_ghc_9_8_3.html)\]

What is GHC?
------------

::: {.body}
GHC is a state-of-the-art, open source compiler and interactive
environment for the functional language
[Haskell](http://www.haskell.org/).

Highlights:

- GHC supports the entire **[Haskell 2010 language][haskell2010]**
  plus a wide variety of **[extensions][]**.

- GHC has particularly good support for **[concurrency][]** and **[parallelism][]**,
  including support for **[Software Transactional Memory][stm]**.

- GHC works on numerous **[platforms][]** including Windows, macOS, Linux, and several
  different processor architectures. There are detailed **[instructions][porting]**
  for porting GHC to new platforms.

- GHC has extensive **[optimisation][]** capabilities, including inter-module optimisation.
  Take a look at GHC\'s perfomance on **[The Computer Language Benchmarks Game][benchmarks]**.

- GHC compiles Haskell programs either directly to native code or by
  using [LLVM](https://llvm.org/) as a back-end. GHC can also generate
  C code as an intermediate target for porting to new platforms. The
  **[interactive environment][ghci]** quickly compiles Haskell to bytecode, and
  supports execution of mixed bytecode/compiled programs.

- **[Profiling][profiling]** is supported, both by time/allocation and various
  kinds of heap profiling.

- GHC comes with several **[libraries][libraries]** and thousands more are available on
  [Hackage](https://hackage.haskell.org/).

- GHC is supported by a great set of **tooling**, from [language servers][hls] to
  [build systems][cabal-install] to [verification tools][liquid], to make writing your program a joy.

- GHC is a **breeze to install** using [`ghcup`][ghcup] or [Stack][]

GHC is heavily dependent on its users and [contributors][].
Please come and join us on [GitLab][], [Matrix][], [Discourse][], or our
[mailing lists](https://wiki.haskell.org/Mailing_lists) to share your
comments, suggestions, bug reports and contributions!

We are an open-source project developed and guided by our contributors.
However, some larger strategic decisions are undertaken by a smaller group of
core contributors. See the [`ghc-hq`](https://gitlab.haskell.org/ghc/ghc-hq)
project for details on our governance structure.
:::

Our Sponsors
------------

GHC development is facilitated by a number of generous sponsors including but not limited to:

- [DreamHost](https://dreamhost.com/)
- [IOG](https://www.iog.io/)
- [Google Research](https://google.com/)
- [MacStadium](https://macstadium.com/)
- [Packet](https://packet.com/)
- [Tweag I/O](https://tweag.io/)
- [Serokell](https://Serokell.io/)
- [Dotcom-Monitor](https://www.dotcom-monitor.com/)
- [LoadView](https://www.loadview-testing.com/)
- [Web Hosting Buddy](https://webhostingbuddy.com/)
- [Find My Electric](https://www.findmyelectric.com/)

::: {.footer style="background: #e0e0e0; margin-top: 30px"}
This site is maintained by [Ben Gamari](mailto:ben@well-typed.com).
Please send me comments, questions and reports of any problems to do
with the site.
:::


[haskell2010]: https://www.haskell.org/haskellwiki/Definition
[extensions]: https://downloads.haskell.org/ghc/latest/docs/html/users_guide/exts.html
[concurrency]: https://hackage.haskell.org/package/base-4.18.0.0/docs/Control-Concurrent.html
[parallelism]: https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/using-concurrent.html#using-smp-parallelism
[porting]: https://gitlab.haskell.org/ghc/ghc/-/wikis/building/porting
[stm]: https://hackage.haskell.org/package/stm
[benchmarks]: https://benchmarksgame-team.pages.debian.net/benchmarksgame/fastest/haskell.html
[platforms]: https://gitlab.haskell.org/ghc/ghc/-/wikis/platforms/
[optimisation]: https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/using-optimisation.html
[ghci]: https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/ghci.html
[profiling]: https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/profiling.html
[libraries]: https://downloads.haskell.org/~ghc/latest/docs/html/libraries/index.html
[contributors]: https://gitlab.haskell.org/ghc/ghc-hq/-/blob/main/team.mkd#the-ghc-team
[hls]: https://github.com/haskell/haskell-language-server
[cabal-install]: https://www.haskell.org/cabal/
[liquid]: https://ucsd-progsys.github.io/liquidhaskell/
[ghcup]: https://www.haskell.org/ghcup/
[Stack]: https://docs.haskellstack.org/en/stable/
[GitLab]: https://gitlab.haskell.org/ghc/ghc
[Matrix]: https://matrix.to/#/#GHC:matrix.org
[Discourse]: https://discourse.haskell.org/
[mailing lists]: https://wiki.haskell.org/Mailing_lists
